TROUBLESHOOTING de Revisión de Servidor
========================================

Revisión de los componentes principales del servidor:
+++++++++++++++++++++++++++++++++++++++++++++++++++++
	
	1) Memoria.
	2) CPU.
	3) Almacenamiento.
	4) Canales de Fibra (Path).
	5) Network. 
	6) Mensajes y/o Errores.
	7) Archivos de configuración.
	

Parametros iniciales:
+++++++++++++++++++++

	.Ingreso con permisos de super usuario (root).
	.Verificar conexiones a consola, actuales.
	.Verificar estatus del servidor, eso incluye tiempo en estado UP.



1) Memoria: 
+++++++++++
	
	Verificar los  valores nominales y porcentuales en uso actualmente, de usuario, procesos y comandos. (%user, %kern, %wait, %idle.). 

	Del mismo modo, verificar la paginación y/o uso de memoria swap.

	Se puede verificar, mediante el uso de::

		nmon, topas, htop, top, glance, etc.

2) CPU:
++++++++

	Verificar los valores nominales y porcentuales en uso actualmente. Notando la cantidad de CPU utilizados, y su ocupación por %user, %sys, %wait, %idle, o procesos que estén consumiendo usos de CPU.

	Se puede verificar, mediante el uso de::

	 	nmon, topas, htop, top, glance, etc.

3) Almacenamiento:
++++++++++++++++++

	Verificar los FileSystem asociados al servidor y su usuo y/o %de ocupación::

		--> df -h   --> df -g   --> cat /etc/fstab (CentOS) --> cat /etc/filesystems (AIX)


	Verificar los Physical Volume (PV), Volume Group (VG), Logical Volume (LV).

	## Comandos de utilidad ---> AIX ##
	+++++++++++++++++++++++++++++++++++

	##################################################################################################
	----:: lsvg    				--(Se utiliza para verificar los VG presentes en el Servidor).
	----:: lsvg -o 				--(VG activos en el servidor).
	----:: lsvg -l nombre_vg    --(Se utiliza para verificar los LV, PV y FileSystems asociados al VG).
	----:: lsvg nombre_vg 		--(Se utiliza para verificar los parametros y recursos del VG).

	----:: lspv  				--(Se utiliza para verificar los PV (Discos) presentes en el servidor).
	----:: lspv nombre_pv       --(Lista las caracteristicas del PV (Discos) ).
	----:: lspv -l nombre_pv	--(Lista los LV y punto de montura asociados al PV).
	##################################################################################################

	## Comandos de utilidad ---> Linux ##
	+++++++++++++++++++++++++++++++++++++

	---:: vgdisplay 				--(Se utiliza para verificar los VG presentes en el Servidor).
	---:: vgdisplay -v nombre_vg    --(Se utiliza para verificar los LV, PV y FileSystems asociados al VG).
	---:: lvdisplay 				--(Se utiliza para verificar los LV presentes en el Servidor y sus caracteristicas).
	##################################################################################################


4) Canales de Fibra (Path).
++++++++++++++++++++++++++++

		1) Enlistar los adaptadores de Fibra::
		++++++++++++++++++++++++++++++++++++++
		
		--> lsdev -Cc adapter |grep fc

		2) Se lista los caminos de fibra. (Atento a cuantos discos se ven por las fibras).
		······································
			. lspath |more
			. lspath |grep -i ENABLE |awk '{print $3}' |sort |uniq -c		

		3) Lista los hijos de la fibre channel
		······································
			. lsdev -C -p fscsi_X

5) Network.
	
	Se verifican las interfaces activas.

		--> ifconfig -a (UNIX).
		--> ip a (Linux).

    Se verifican las rutas estáticas.

    	--> netstat -nr 

    Se verifican las conexiones mediante ICMP (ping), se puede utilizar la herramienta --> tcpdump, para verificar el tráfico específico por una red específica.

    Se verifican los archivos /etc/hosts y los archivos de configuración de interfaz (/etc/sysconfig/network-scripts/ifcfg-X).

	##################################################################################################


6) Mensajes y/o Errores.

		Verificar logs del sistema:

		AIX . Se utiliza el comando ---> errpt |more 
		Linux . Se utiliza el comando ---> tail -100f /var/log/messages.


7) Archivos de configuración.

	Se verifican los archivos de configuración del servidor.

	--> hosts, fstab, group, passwd, shadow, filesystems.


	NOTA: Es importante revisar los archivos solo con --> cat ; y no modificarlos "Solo si es estrictamente necesario" y antes de hacerlo, es sana practica respaldar el archivo antes de modificarlo.

·································································································

La presente GUIA es solo una herramienta Básica para la revisión de cualquier servidor, y la resolución de problemas solo dependerá del tipo de error que presente el server.

Buenas Practicas son:

	1) Respaldar archivos importantes para la operación del S.O.

		/etc/hosts
		/etc/filesystems
		/etc/passwd
		/etc/group
		/etc/shadows
		/etc/fstab
		/etc/sudoers

	2) Guardar salidas a comandos importantes:

		lsvg > vg_actuales_fecha.txt
		lspv > pv_actuales_fecha.txt
		df -g > filesystem_mount_fecha.txt
		hostsname > hostsname_fecha.txt
		lvdisplay > lv_actuales_fecha.txt
		vgdisplay > vg_actuales_fecha.txt
		netstat -nr > rutas_fecha.txt
		lsblk -fm > discos_fecha.txt
		crontab -l > crontab_fecha.txt
		lsdev -Cc adapter |grep fc > dispositivos_fibra_fecha.txt
		lspath > rutas_fecha.txt
		uname -a > uname_a.txt
		oslevel -s > oslevel_s.txt
		uptime > uptime.txt
		df -h > filesystem_mount_fecha.txt
		ifconfig -a > ifconfig_a.txt
		lssrc -a > lssrc_a.txt
		errpt -a > errpt_a.txt
		cp /etc/inittab ./inittab.txt
		
		############################################################################################
		####################################################################################T.A.D.M.
